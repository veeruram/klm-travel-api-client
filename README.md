Travel API Client 
=================
Pre Requesite

Lombok
Travel API Mock Service Up & Running on localhost:8080

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

Dashboard View

[http://localhost:9090/travel/Dashboard.html](http://localhost:9090/travel/Dashboard.html)


Flight Fare search

[http://localhost:9090/travel/index.html](http://localhost:9090/travel/index.html)

Spring Boot Admin for viewing the metrics graphically

[http://http://localhost:9090/travel/](http://localhost:9090/travel/)

App metrics

http://localhost:9090/travel/actuator/metrics/
http://localhost:9090/travel/actuator/metrics/http.server.requests
http://localhost:9090/travel/actuator/metrics/http.server.requests?uri:/fare
http://localhost:9090/travel/actuator/metrics/http.server.requests?uri:/airports

Status Counts

http://localhost:9090/travel/actuator/metrics/http.server.requests?uri:/fare&tag=status:200 
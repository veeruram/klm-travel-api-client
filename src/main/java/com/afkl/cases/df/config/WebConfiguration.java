package com.afkl.cases.df.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

	
	  @Override public void addResourceHandlers(ResourceHandlerRegistry registry) {
	  registry.addResourceHandler("/index.html").addResourceLocations(
	  "classpath:/WEB-INF/index.html");
	  registry.addResourceHandler("/Dashboard.html").addResourceLocations(
	  "classpath:/WEB-INF/Dashboard.html");
	  
	  }
	 

	

}

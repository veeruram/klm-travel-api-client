package com.afkl.cases.df.util;


public final class Constants {
	
	private Constants() {}
	public static final String SEARCH_TERM= "term";
	public static final String MAPPING_ERROR = "error.mappingError";
	public static final String SERVER_ERROR_CODE = "500";
	public static final String SEARCH_SIZE = "size";
	public static final String SEARCH_PAGE = "page";

}

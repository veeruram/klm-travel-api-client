package com.afkl.cases.df.util;


public enum RestResource {

	GET_ALL_AIRPORTS("/airports"), 
	GET_FARE("/fares");
	

	private final String name;

	RestResource(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}

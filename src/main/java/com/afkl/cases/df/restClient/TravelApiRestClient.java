package com.afkl.cases.df.restClient;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;

import com.afkl.cases.df.model.response.AirportsResponse;
import com.afkl.cases.df.model.response.Fare;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.exception.TravelApiRestClientException;

public interface TravelApiRestClient {

	AirportsResponse getEntities(String query) throws TravelApiRestClientException;

	Fare getFare(String origin, String destination) throws TravelApiRestClientException;

	@Async
	CompletableFuture<Location> getLocationByCode(String code) throws TravelApiRestClientException;

	AirportsResponse getAirports(String term, Long size, Long pageNumber) throws TravelApiRestClientException;

}

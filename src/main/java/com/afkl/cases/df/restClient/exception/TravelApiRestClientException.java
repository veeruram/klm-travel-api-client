package com.afkl.cases.df.restClient.exception;


public class TravelApiRestClientException extends Exception {

  private static final long serialVersionUID = 1811984843526892548L;

  private String errorMessage;
  private String errorCode;
  private String errorDesc;


  public TravelApiRestClientException(Throwable cause) {
    super(cause);
    this.errorMessage = cause.getMessage();
  }

  public TravelApiRestClientException(String message) {
    super(message);
    this.errorMessage = message;
  }

  public TravelApiRestClientException(String message, Throwable cause) {
    super(message, cause);
    this.errorMessage = message;
  }

  public TravelApiRestClientException(String message, String code) {
    super(message);
    this.errorMessage = message;
    this.errorCode = code;
  }
  
  public TravelApiRestClientException(String message, String code, String errorDesc) {
    super(message);
    this.errorMessage = message;
    this.errorCode = code;
    this.errorDesc = errorDesc;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorDesc() {
    return errorDesc;
  }

  public void setErrorDesc(String errorDesc) {
    this.errorDesc = errorDesc;
  }

}

package com.afkl.cases.df.restClient.impl;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.model.response.AirportsResponse;
import com.afkl.cases.df.model.response.Fare;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.TravelApiRestClient;
import com.afkl.cases.df.restClient.exception.TravelApiRestClientException;
import com.afkl.cases.df.util.Constants;
import com.afkl.cases.df.util.RestResource;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class TravelApiRestClientImpl implements TravelApiRestClient {

	@NonNull
	protected RestTemplate restTemplate;

	protected final ObjectMapper mapper = new ObjectMapper();

	@Value("${travel.api.url}")
	String travelApiUrl;

	@Override
	public AirportsResponse getEntities(String query) throws TravelApiRestClientException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(travelApiUrl + "/" + RestResource.GET_ALL_AIRPORTS.getName())
				.queryParam(Constants.SEARCH_TERM, query);
		
		return restTemplate.getForObject(builder.buildAndExpand().toUri(), AirportsResponse.class);

	}

	@Override
	public Fare getFare(String origin, String destination) throws TravelApiRestClientException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(travelApiUrl + "/" + RestResource.GET_FARE.getName()).path("/" + origin)
				.path("/" + destination);

		return restTemplate.getForObject(builder.buildAndExpand().toUri(), Fare.class);
	}

	@Override
	public CompletableFuture<Location> getLocationByCode(String code) throws TravelApiRestClientException {

		Location response;
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(travelApiUrl + "/" + RestResource.GET_ALL_AIRPORTS.getName()).path("/" + code);

		response = restTemplate.getForObject(builder.buildAndExpand().toUri(), Location.class);

		return CompletableFuture.completedFuture(response);

	}

	@Override
	public AirportsResponse getAirports(String term, Long size, Long pageNumber) throws TravelApiRestClientException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromUriString(travelApiUrl + "/" + RestResource.GET_ALL_AIRPORTS.getName())
				.queryParam(Constants.SEARCH_TERM, term).queryParam(Constants.SEARCH_SIZE, size)
				.queryParam(Constants.SEARCH_PAGE, pageNumber);

		return restTemplate.getForObject(builder.buildAndExpand().toUri(), AirportsResponse.class);

	}

}

package com.afkl.cases.df.service.impl;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.afkl.cases.df.model.response.AirportsResponse;
import com.afkl.cases.df.model.response.DataTableResponse;
import com.afkl.cases.df.model.response.Fare;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.TravelApiRestClient;
import com.afkl.cases.df.restClient.exception.TravelApiRestClientException;
import com.afkl.cases.df.service.AirportsService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
public class AirportsServiceImpl implements AirportsService {
	
	@NonNull
	TravelApiRestClient travelApiRestClient;
	
	@Override
	public List<Location> getAirportsLocation(String query) throws TravelApiRestClientException {
		
		return travelApiRestClient.getEntities(query).getEmbedded().getLocations();
	}

	@Override
	public Fare getFare(String origin, String destination) throws TravelApiRestClientException, InterruptedException, ExecutionException {
		Fare fare = travelApiRestClient.getFare(origin, destination);
		if(null!=fare) {
		CompletableFuture<Location> originDetails = travelApiRestClient.getLocationByCode(fare.getOrigin());
		CompletableFuture<Location> destinationDetails = travelApiRestClient.getLocationByCode(fare.getDestination());
		CompletableFuture.allOf(originDetails,destinationDetails).join();
		fare.setOrigin(originDetails.get().getDescription());
		fare.setDestination(destinationDetails.get().getDescription());
	    log.info("--> " + originDetails.get().toString());
	    log.info("--> " + destinationDetails.get().toString());
		}
		return fare;
	}

	@Override
	public DataTableResponse getAirportTableResponse(Long start, Long length, Long draw, String term)
			throws TravelApiRestClientException {
		long pageNumber = start!=0?start/length+1:1;
		AirportsResponse airports = travelApiRestClient.getAirports(term, length, pageNumber);
		return DataTableResponse.builder().draw(draw).recordsFiltered(Long.valueOf(airports.getPage().getTotalElements())).recordsTotal(Long.valueOf(airports.getPage().getTotalElements())).data(airports.getEmbedded().getLocations()).build();
		}



}

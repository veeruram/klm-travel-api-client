package com.afkl.cases.df.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.afkl.cases.df.model.response.DataTableResponse;
import com.afkl.cases.df.model.response.Fare;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.exception.TravelApiRestClientException;

public interface AirportsService {
	
	List<Location> getAirportsLocation(String query) throws TravelApiRestClientException;

	Fare getFare(String origin, String destination) throws TravelApiRestClientException, InterruptedException, ExecutionException;
	
	DataTableResponse getAirportTableResponse(Long start, Long length, Long draw, String term) throws TravelApiRestClientException;


}

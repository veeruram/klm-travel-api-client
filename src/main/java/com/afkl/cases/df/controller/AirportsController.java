package com.afkl.cases.df.controller;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.model.response.DataTableResponse;
import com.afkl.cases.df.model.response.Fare;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.exception.TravelApiRestClientException;
import com.afkl.cases.df.service.AirportsService;

import io.micrometer.core.annotation.Timed;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@Timed
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class AirportsController {

	@NonNull
	AirportsService airportService;

	@GetMapping("/airports")
	public List<Location> getAirportsResponse(@RequestParam(name = "q") String search)
			throws TravelApiRestClientException {

		return airportService.getAirportsLocation(search);

	}

	@GetMapping("/airports/dashboard")
	public DataTableResponse getAirportTableData(@RequestParam(name = "draw") Long draw,
			@RequestParam(name = "start") Long start, @RequestParam(name = "length") Long length,
			@RequestParam(required = false, name = "search[value]") String search) throws TravelApiRestClientException {

		return airportService.getAirportTableResponse(start, length, draw, search);

	}

	@GetMapping("/fare")
	public Fare getFare(@RequestParam(name = "origin") String origin,
			@RequestParam(name = "destination") String destination)
			throws TravelApiRestClientException, InterruptedException, ExecutionException {

		return airportService.getFare(origin, destination);

	}

}

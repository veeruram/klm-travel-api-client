
package com.afkl.cases.df.model.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "locations"
})
@Getter
@Setter
@NoArgsConstructor
public class Embedded {

    @JsonProperty("locations")
    public List<Location> locations = null;

}


package com.afkl.cases.df.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_embedded",
    "page"
})
@Getter
@Setter
@NoArgsConstructor
public class AirportsResponse {

    @JsonProperty("_embedded")
    public Embedded embedded;
    @JsonProperty("page")
    public Page page;

}

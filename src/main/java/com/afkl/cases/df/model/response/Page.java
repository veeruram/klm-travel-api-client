
package com.afkl.cases.df.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "size",
    "totalElements",
    "totalPages",
    "number"
})
@Getter
@Setter
@NoArgsConstructor
public class Page {

    @JsonProperty("size")
    public Integer size;
    @JsonProperty("totalElements")
    public Integer totalElements;
    @JsonProperty("totalPages")
    public Integer totalPages;
    @JsonProperty("number")
    public Integer number;

}

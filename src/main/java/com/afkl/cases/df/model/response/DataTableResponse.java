
package com.afkl.cases.df.model.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "draw",
    "recordsTotal",
    "recordsFiltered",
    "data"
})
@Getter@Setter@Builder
@AllArgsConstructor
public class DataTableResponse {

	
    public DataTableResponse() {
	}
	@JsonProperty("draw")
    public Long draw;
    @JsonProperty("recordsTotal")
    public Long recordsTotal;
    @JsonProperty("recordsFiltered")
    public Long recordsFiltered;
    @JsonProperty("data")
    public List<Location> data;

}


package com.afkl.cases.df.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "name",
    "description",
    "coordinates",
    "parent"
})
@Getter
@Setter
@NoArgsConstructor
public class Parent {

    @JsonProperty("code")
    public String code;
    @JsonProperty("name")
    public String name;
    @JsonProperty("description")
    public String description;
    @JsonProperty("coordinates")
    public Coordinates coordinates;
    @JsonProperty("parent")
    public Parent parentTree;

}

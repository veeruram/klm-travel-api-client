package com.afkl.cases.df.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.nio.file.Files;
import java.util.concurrent.CompletableFuture;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;

import com.afkl.cases.df.model.response.AirportsResponse;
import com.afkl.cases.df.model.response.DataTableResponse;
import com.afkl.cases.df.model.response.Location;
import com.afkl.cases.df.restClient.TravelApiRestClient;
import com.afkl.cases.df.service.AirportsService;
import com.afkl.cases.df.model.response.Fare;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(value = AirportsController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class AirportsControllerTest {
	
	@Autowired
    private MockMvc mvc;

    @MockBean
    private TravelApiRestClient travelApiRestClient;
    
    @MockBean
    private AirportsService service;

    @Before
    public void setUp() throws Exception {
    	File dataTableResp = ResourceUtils.getFile("classpath:mock-data/dashboard-location-mock.json");
		String dataTableRespJson = new String(Files.readAllBytes(dataTableResp.toPath()));
		DataTableResponse tableResponse = new ObjectMapper().readValue(dataTableRespJson, DataTableResponse.class);
		when(service.getAirportTableResponse(0L, 10L, 1L, "MAA")).thenReturn(tableResponse);
    	
		File fareResp = ResourceUtils.getFile("classpath:mock-data/fare-calculation-mock.json");
		String fareRespJson = new String(Files.readAllBytes(fareResp.toPath()));
		Fare fare = new ObjectMapper().readValue(fareRespJson, Fare.class);
		when(service.getFare("MAA", "AMS")).thenReturn(fare);
    	
		
    	File airportsResponseJson = ResourceUtils.getFile("classpath:mock-data/airports-response-mock.json");
		String jsonContent = new String(Files.readAllBytes(airportsResponseJson.toPath()));
		AirportsResponse airportsResponse = new ObjectMapper().readValue(jsonContent, AirportsResponse.class);  
    	when(travelApiRestClient.getAirports("MAA", 10L, 1L)).thenReturn(airportsResponse);
		
    	File amsLocationJson = ResourceUtils.getFile("classpath:mock-data/ams-location-mock.json");
		String amsJsonContent = new String(Files.readAllBytes(amsLocationJson.toPath()));
		Location ams = new ObjectMapper().readValue(amsJsonContent, Location.class);  
    	when(travelApiRestClient.getLocationByCode("AMS")).thenReturn(CompletableFuture.completedFuture(ams));
		
    	File maaLocationJson = ResourceUtils.getFile("classpath:mock-data/maa-location-mock.json");
		String maaJsonContent = new String(Files.readAllBytes(maaLocationJson.toPath()));
		Location maa = new ObjectMapper().readValue(maaJsonContent, Location.class);  
    	when(travelApiRestClient.getLocationByCode("MAA")).thenReturn(CompletableFuture.completedFuture(maa));
		
    	File fareJson = ResourceUtils.getFile("classpath:mock-data/fare-response-mock.json");
		String fareJsonContent = new String(Files.readAllBytes(fareJson.toPath()));
		Fare fareResponse = new ObjectMapper().readValue(fareJsonContent, Fare.class);  
    	when(travelApiRestClient.getFare("MAA", "AMS")).thenReturn(fareResponse);
    	
    	
    }
	
	
	
	@Test
	public void getAirportsController_ShouldReturnOkStatusWhenEndPointIsCorrect() throws Exception {
		
		File response = ResourceUtils.getFile("classpath:mock-data/dashboard-location-mock.json");
		String jsonContent = new String(Files.readAllBytes(response.toPath()));
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("draw", "1");
		params.add("start", "0");
		params.add("length", "10");
		params.add("search[value]", "MAA");
		mvc.perform(
				MockMvcRequestBuilders.get("/airports/dashboard").params(params)
						.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().json(jsonContent));
	}
	



	@Test
	public void getFare_ShouldReturnOkStatusWhenEndPointIsCorrect() throws Exception {
		File response = ResourceUtils.getFile("classpath:mock-data/fare-calculation-mock.json");
		String jsonContent = new String(Files.readAllBytes(response.toPath()));
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("origin", "MAA");
		params.add("destination", "AMS");
		mvc.perform(
				MockMvcRequestBuilders.get("/fare").params(params)
						.accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().json(jsonContent));
	}


}
